README
======

Temperature/Humidity/Dew Point
------------------------------

Data are collected via root cron like this::

    * * * * * /usr/bin/python /home/pi/wikihouse/collect_data.py


You have to start grafana-server manually after boot.

In a terminal::

    sudo service grafana-server start


Check with status::

    sudo service grafana-server status


Open grafana in browser - wikihouse: 

http://localhost:3000/dashboard/db/wikihouse


Currently sensors are connected on pins 11, 13, 15 (BCM 17,27,22).

More sensors can be connected as per collect_data.py

For pinout see https://pinout.xyz


A/D Converter
-------------

Run in a terminal::

    cd wikihouse
    python read_adc.py


If you need to change the way how this works, edit read_adc.py and
change the following settings::

    # sample rate ie. sleep time between readings...
    SAMPLE_RATE = 0.1

    # how long we going to run the experiment for in seconds?
    EXPERIMENT_LENGTH = 60.0

Maximum input voltage is 4.096V!

import Adafruit_DHT

from datetime import datetime
from influxdb import InfluxDBClient
from influxdb.client import InfluxDBClientError

#pin = 23
pin1 = 17
pin2 = 27
pin3 = 22

client = InfluxDBClient('localhost', 8086, 'root', 'root', 'wikihouse')

def save_data(sensor, pin):
    current_time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    humidity, temperature = Adafruit_DHT.read_retry(Adafruit_DHT.DHT22, pin) 
    if humidity is not None and temperature is not None:
        print('Sensor={0}  Temp={1:0.1f}*C  Humidity={2:0.1f}%'.format(sensor, temperature, humidity))
        json_body = [
            {
                "measurement": "{0}_temperature".format(sensor),
                "tags": {
                    "wikihouse": "festa"
                },
                "timestamp": current_time,
                "fields": {
                    "value": float("{0:0.1f}".format(temperature))
                }
            },
            {
                "measurement": "{0}_humidity".format(sensor),
                "tags": {
                    "wikihouse": "festa"
                },
                "timestamp": current_time,
                "fields": {
                    "value": float("{0:0.1f}".format(humidity))
                }
            }]
        #print(json_body)
        client.write_points(json_body)

#save_data('inside', pin)
save_data('inside', pin1)
save_data('inwall', pin2)
save_data('outside', pin3)

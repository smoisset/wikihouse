import time
import Adafruit_ADS1x15
from datetime import datetime
from influxdb import InfluxDBClient

# setup influxdb client
client = InfluxDBClient('localhost', 8086, 'root', 'root', 'wikihouse')
# setup A/d 16 bit converter
adc = Adafruit_ADS1x15.ADS1115()
# Choose a gain of 1 for reading voltages from 0 to 4.09V.
# Or pick a different gain to change the range of voltages that are read:
#  - 2/3 = +/-6.144V
#  -   1 = +/-4.096V
#  -   2 = +/-2.048V
#  -   4 = +/-1.024V
#  -   8 = +/-0.512V
#  -  16 = +/-0.256V
# See table 3 in the ADS1015/ADS1115 datasheet for more info on gain.
GAIN = 1
adc.start_adc(0, gain=GAIN)

# sample rate ie. sleep time between readings...
SAMPLE_RATE = 0.1

# how long we going to run the experiment for in seconds?
EXPERIMENT_LENGTH = 60.0

# transfer function conversion value for the load cells,
# what is the cell voltage for 1 kN?
TF_CONVERSION = 1.0


def save_data_to_influxdb(data):
    current_time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    print('Reading: {}'.format(data))
    json_body = [
        {
            "measurement": "load",
            "tags": {
                "wikihouse": "loadtest"
            },
            "timestamp": current_time,
            "fields": {
                "value": int(data)
            }
        },
        {
            "measurement": "load_float",
            "tags": {
                "wikihouse": "loadtest"
            },
            "timestamp": current_time,
            "fields": {
                "value": float(data)
            }
        }
        ]
    # print(json_body)
    client.write_points(json_body)


print('Reading A/D converter for 1 minute...')
start = time.time()
while (time.time() - start) <= EXPERIMENT_LENGTH:
    value = adc.get_last_result()
    load = value * 4.096 / 32767.0 / TF_CONVERSION
    save_data_to_influxdb(load)
    time.sleep(SAMPLE_RATE)
adc.stop_adc()
